var express = require('express');
var Fact = require('../db/models/fact.js');
var User = require('../db/models/user.js');
var Game = require('../db/models/game.js');
var Log = require('../db/models/log.js');
var api = express.Router();

/* GET home page. */
api.get('/', function(req, res, next) {
  res.send({
    'hello': 'world'
  });
});

api.route('/fact')
  .get(function(req, res, next) {
    Fact.fetch()
      .then((fact) => {
        let response = { 
          result: 'success',
          fact: fact
        }
        res.send(response);
      })
  });

api.route('/users')
  .post(function(req, res, next) {
    User.create({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email
    }).then((user) => {
      let response = { 
        result: 'success',
        user: user
      }
      res.send(response);
    });
  })

api.get('/game/sessions/start', function(req, res, next) {
  if (req.query.user) {
    let session = Game.start(req.query.user);
    res.send({ 
      result: 'success',
      session: session
    })
  } else {
    res.status(422).send({
      result: 'error',
      message: 'No user uuid specified'
    })
  }
  
});

api.post('/game/sessions/:sessionId', function(req, res, next) {
  let session = Game.stop(req.params.sessionId, req.body.score);
  let response = { 
    result: 'success',
    session: session
  }
  res.send(response);
});

api.get('/game/scores', function(req, res, next) {
  Game.fetchScores(req.query.limit)
    .then((scores) => {
      let response = { 
        result: 'success',
        scores: scores
      }
      res.send(response);
    })
});

api.post('/log', function(req, res, next) {
  let logEntry = Log.create({
    user: req.body.user,
    action: req.body.action
  });

  let response = { 
    result: 'success',
    logEntry: logEntry
  }
  res.send(response);
});
api.get('*', function(req, res, next) {
  res.send({
    'error': '404'
  });
})
module.exports = api;
