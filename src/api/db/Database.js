var sqlite3 = require('sqlite3').verbose();


class Database {

  init(path) {
    this.db = new sqlite3.Database(path)
  }

  install() {
    this.db.run('CREATE TABLE IF NOT EXISTS "facts" ("uuid" varchar PRIMARY KEY NOT NULL,"fact" text(128));');
    this.db.run('CREATE TABLE IF NOT EXISTS "games" ("uuid" varchar(36) PRIMARY KEY NOT NULL, "user_uuid" varchar(128), "start" integer, "end" integer, "score" integer);');
    this.db.run('CREATE TABLE IF NOT EXISTS "log" ("uuid" varchar(36) PRIMARY KEY NOT NULL, "user_uuid" varchar(36), "action" char(128), "time" integer(128));');
    this.db.run('CREATE TABLE IF NOT EXISTS "users" ("uuid" varchar(36) PRIMARY KEY NOT NULL, "first_name" char(48), "last_name" char(48), "email" char(128));');
  }
}

const _db = new Database()

module.exports = _db;