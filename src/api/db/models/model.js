let Database = require('../Database.js')
let faker = require('faker')
let moment = require('moment')
let uuid = require('uuid/v4')


class Model {
  constructor () {
    this.db = Database.db
    this.now = new moment()
    this.faker = faker
  }
  generateUuid () {
    return uuid();
  }
}

module.exports = Model;