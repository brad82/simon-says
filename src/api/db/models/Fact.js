var Model = require('./model.js')

class Fact extends Model {
  fetch(uuid) {
    return new Promise((resolve, reject) => {
      this.db.get('SELECT * FROM facts ORDER BY RANDOM() LIMIT 1', (err, row) => {
        if(err) {
          reject(err)
        } else {
          resolve({
            uuid: row.uuid,
            text: row.fact
          })
        }
      })
    })
  }
}

module.exports = new Fact();
