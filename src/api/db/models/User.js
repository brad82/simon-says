var Model = require('./model.js')

class User extends Model {
  fetch(uuid) {
    return {
      uuid: this.generateUuid(),
      first_name: this.faker.name.firstName(),
      last_name: this.faker.name.lastName(),
      email: this.faker.internet.email(),
    } 
  }
  create(data) {
    
    let query = this.db.prepare('INSERT INTO users (uuid, first_name, last_name, email) VALUES (?, ?, ?, ?)');
    let user = {
      uuid: this.generateUuid(),
      first_name: data.firstName,
      last_name: data.lastName,
      email: data.email,
    }

    return new Promise((resolve, reject) => {
      query.run(user.uuid, user.first_name, user.last_name, user.email, (err) => {
        if (err) {
          return reject(err)
        }
        resolve(user)
      })
    })
  }
}
module.exports = new User();
