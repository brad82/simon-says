var map = require('lodash/map')
var Model = require('./model.js')

class Game extends Model {
  start(userUuid) {
    var query = this.db.prepare('INSERT INTO games (uuid, start, user_uuid) VALUES (?, ?, ?)')
    
    let session = {
      uuid: this.generateUuid(),
      start: this.now.unix(),
      user_uuid: userUuid
    }

    query.run(session.uuid, session.start, session.user_uuid)

    return session
  }

  stop(gameUuid, score) {
    var query = this.db.prepare('UPDATE games SET end = ?, score = ? WHERE uuid = ?')
    query.run(this.now.unix(), score, gameUuid)
    return true;
  }

  fetchScores (limit = -1) {
    var scores = [];
    var query = 'SELECT'+
      ' games.uuid AS game_uuid,'+
      ' games.score,'+
      ' games.user_uuid,'+
      ' users.uuid,'+
      ' users.first_name,'+
      ' users.last_name'+
      ' FROM games'+
      ' LEFT JOIN users on games.user_uuid = users.uuid'+
      ' WHERE games.user_uuid NOT NULL AND games.score NOT NULL'+
      ' ORDER BY score DESC';
    if (limit > -1) {
      query += ' LIMIT ' + limit
    }
    return new Promise((resolve, reject) => {
      this.db.all(query, function(err, rows) {
        if(err) {
          reject(err)
        } else {
          resolve(map(rows, (row) => {
            return {
              uuid: row.game_uuid,
              score: row.score,
              user: {
                uuid: row.user_uuid,
                first_name: row.first_name,
                last_name: row.last_name,
              }
            }
          }))
        }
      })
    })
  }
}

module.exports = new Game();
