var Model = require('./model.js')

class Log extends Model {
  create(data) {
    let query = this.db.prepare('INSERT INTO log (uuid, action, time, user_uuid) VALUES (?, ?, ?, ?)');
    let logEntry = {
      uuid: this.generateUuid(),
      user_uuid: data.user,
      action: data.action,
      timestamp: this.now.unix()
    }
    return new Promise((resolve, reject) => {
      query.run(logEntry.uuid, logEntry.action, logEntry.timestamp, logEntry.user_uuid, (err, result) => {
        if (err) {
          return reject(err)
        }
        resolve(result)
      })
    })
  }
}
module.exports = new Log();
