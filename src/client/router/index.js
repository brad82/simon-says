import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/views/Welcome'
import Register from '@/views/Register'
import Fact from '@/views/Fact'
import Game from '@/views/Game'
import GameOver from '@/views/GameOver'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/fact',
      name: 'Fact',
      component: Fact
    },
    {
      path: '/game',
      name: 'Game',
      component: Game
    },
    {
      path: '/game-over',
      name: 'GameOver',
      component: GameOver
    }
  ]
})
