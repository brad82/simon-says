import axios from 'axios'

let API_URL = 'http://localhost:3000/api/v1'

export default {
  users: {
    create: function (user) {
      return axios.post(API_URL + '/users', user)
        .then(response => response.data.user)
    }
  },
  fact: {
    fetch: function () {
      return axios.get(API_URL + '/fact')
        .then(response => response.data.fact)
    }
  },
  game: {
    start: function (user) {
      return axios.get(API_URL + '/game/sessions/start', {
        params: { user: user }
      }).then(response => response.data.session)
    },
    finish: function (game) {
      return axios.post(API_URL + '/game/sessions/' + game.uuid, {
        score: game.score
      })
    },
    scores: function (args) {
      return axios.get(API_URL + '/game/scores/', {
        params: args
      }).then(response => response.data)
    }
  },
  log: {
    record: function (entry) {
      return axios.post(API_URL + '/log', entry)
    }
  }
}
