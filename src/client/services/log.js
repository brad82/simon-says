import Api from '@/services/api.js'
import Store from '@/services/store.js'

export default {
  record (action) {
    let user = Store.get('user.uuid')
    if (user) {
      Api.log.record({
        user: user,
        action: action
      })
    }
  }
}
