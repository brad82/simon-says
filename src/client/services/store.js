import {get, set, has} from 'lodash'
const store = {}

export default {
  save (key, value) {
    return new Promise((resolve, reject) => {
      set(store, key, value)
      resolve({key: value})
    })
  },
  set (key, value) {
    console.warn('DEPRECIATED Store.save: User Store.save(key, value) instead')
    return new Promise((resolve, reject) => {
      set(store, key, value)
      resolve({key: value})
    })
  },
  get (key, defaultValue) {
    return get(store, key, defaultValue)
  },
  has (key) {
    return has(store, key)
  },
  clear (key) {
    set(store, key, null)
  }
}
